namespace API.Dtos.EventHeads
{
    public class DataForDeletingEventHeadDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}