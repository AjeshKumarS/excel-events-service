namespace API.Dtos.EventHeads
{
    public class DataForAddingEventHead
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
