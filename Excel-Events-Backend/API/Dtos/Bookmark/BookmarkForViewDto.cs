namespace API.Dtos.Bookmark
{
    public class BookmarkForViewDto
    {
        public int Id { get; set; }
        public int ExcelId { get; set; }
        public int EventId { get; set; } 
    }
}